/*
	
	> Mongoose ODM

	> Schema 
	- A representation of a document's structure; also contains a doc's expected properties and data types
	
	> Models
	- A programming interface for querying or manipulating databases. A Mongoose model contains methods that simplify such operations.

*/

const express = require('express');

// Mongoose is a package that allows us to create schemas to model our data structures and to manipulate our databases using different access methods. 
const mongoose = require('mongoose');

const port = 3001;

const app = express();

// [Section] MongoDB connection
	/*
		Syntax:
			mongoose.connect("mongoDBconnectionstring", {options to avoid errors in our connection})
	*/

mongoose.connect("mongodb+srv://admin:admin@batch245-nava.y30w9yr.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
	{
		// Allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

// Error handling in connecting 
db.on("error", console.error.bind(console, "Connection error"));

// Will be triggered if the connection is successful
db.once("open", () => console.log(`We're connected to the cloud database.`));

// Mongoose Schemas
	// Schemas determine the structure of the documents to be written in the database
	// In other words, schemas act as the blueprint to our data
	/*
		Syntax:
			const schemaName = new mongoose.Schema({key: value});
	*/

	// taskSchema currently contains two properties: name and status
	const taskSchema = new mongoose.Schema({
		name: {
			type: String,
			// "required" specifies that a field must not be empty
			required: [true, "Task name is required!"]
		},
		status: {
			type: String,
			// Since we have not required "status" to be filed out, we have set a "default" status, which will show as "pending"
			default: "pending"
		}
	});





// [[[SCHEMA for User Registration]]]

	const userSchema = new mongoose.Schema({
		username: {
			type: String,
			required: [true, "Please select a username."]
		},
		password: {
			type: String,
			required: [true, "Please set a password."]
		}
	});





// [Section] Models
	// Models use schema to create or instatiate documents or objects that follow our schema structure.
		// The variable/object that will be created and be used to run commands with our database
	/*
		Syntax: 
			const variableName = mongoose.model("collectionName", schemaName);
	*/
	const Task = mongoose.model("Task", taskSchema);




// [[[MODEL for User Registration]]]

	const User = mongoose.model("User", userSchema);




// middlewares
app.use(express.json()); // Again, allows the app to read JSON data
app.use(express.urlencoded({extended: true})); // Allows our app to read data from forms

// [Sections] Routing
	// Create/add new task to database
	// 1. Can check if the task already exists
		// If task already exists in the database, the app will return a message saying "The task already exists!"
		// If the task does not exist in the database, we will add it into the database
	app.post("/tasks", (request, response) => {
		let input = request.body;

			console.log(input.status);
			if(input.status === undefined){
				Task.findOne({name: input.name}, (error, result) => {
					console.log(result);
					if( result !== null ){
						return response.send("The task already exists!");
					} else {
						let newTask = new Task({
							name: input.name
						})
						// .save() method will save the object in the collection that the object instatiated
						newTask.save((saveError, savedTask) => {
							if(saveError){
								return console.log(saveError);
							} else {
								return response.send("New task created.");
							}
						});
					}
				});
			} else {
				Task.findOne({name: input.name}, (error, result) => {
					console.log(result);
					if( result !== null ){
						return response.send("The task already exists!");
					} else {
						let newTask = new Task({
							name: input.name,
							status: input.status
						})
						// .save() method will save the object in the collection that the object instatiated
						newTask.save((saveError, savedTask) => {
							if(saveError){
								return console.log(saveError);
							} else {
								return response.send("New task created.");
							}
						});
					}
				});
			}
	});

	// Retrieving
	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error){
				console.log(error);
			} else {
				return response.send(result);
			}
		});
	});




// [[[Route for registering user]]]

	app.post("/signup", (req, resp) => {

		let input = req.body;

		User.findOne({username: input.username}, (error, result) => {
			
			if(result !== null){
				return resp.send("This username is already taken.");
			} else {
				let newUser = new User({
					username: input.username,
					password: input.password
				});

				newUser.save((saveError, savedUser) => {
					if(saveError){
						return console.log(saveError);
					} else {
						return resp.send("Registration successful!");
					}
				});
			}
		})
	});

app.listen(port, ()=>console.log(`Server is running at port ${port}.`));